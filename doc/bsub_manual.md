## 删除作业

bkill 55167

## 用bswitch将正在运行的作业调度到其他队列中
$bswitch MID 55179
Job <55179> is switched to queue <MID>

## 10.5	查看队列状态（bqueues）
$ bqueues
QUEUE_NAME      PRIO STATUS          MAX JL/U JL/P JL/H NJOBS  PEND   RUN  SUSP 
normal           30  Open:Active       -    -    -    -     0     0     0     0

bqueues –l: 查询某个队列的详细信息
`bqueues -l normal`

```R
# QUEUE_NAME      PRIO STATUS          MAX JL/U JL/P JL/H NJOBS  PEND   RUN  SUSP 
# admin            50  Open:Active       -    -    -    -     0     0     0     0
# normal           30  Open:Active       -    -    -    -   263     7   256     0
# interactive      30  Open:Active       -    -    -    -     0     0     0     0
# Q7F32            30  Open:Active       -    -    -    -     0     0     0     0
# Q7662            30  Open:Active       -    -    -    -     0     0     0     0

## References
# https://www.hpc.dtu.dk/?page_id=2723
```

# bkill 2938
# btop 55181 

# 10.5	查看队列状态（bqueues）
# 10.6	查询系统各主机状态（bhosts）
# 10.7	查询各主机系统状态 lsload

```R
# R_exe=/opt/R/4.1.0/bin/R
# export R_BATCH_OPTIONS="--no-save"
# /opt/R/4.1.0/lib/R/bin/Rscript "hello.R"

```
